var express = require('express');
var router = express.Router();
var multer = require('multer');
var upload = multer({dest: './uploads'});
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var User = require('../models/user');


router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/register', function(req, res, next) {
  res.render('register', {title: "Register"});
});

router.get('/login', function(req, res, next) {
  res.render('login', {title: "Login"});
});

router.post('/login', upload.none(),
  passport.authenticate('local', {failureRedirect: '/users/login', failureFlash: 'Invalid Usarname or password' }),
  function(req, res) {
    req.flash('success', `Logged In as ${req.user.name}`);
    res.redirect('/');
});



passport.use(new LocalStrategy(function(username, password, done) {
  User.getUserByUsername(username, function(err, user) {
    if(err) throw err;
    if(!user){
      return done(null, false, {message: 'Unknown user'});
    }

    User.comparePassword(password, user.password, function(err, isMatch) {
      if(err) return done(err);
      if(isMatch){
        return done(null, user);
      }else{
        return done(null, false, {message: 'Invalid Password'})
      }
    });
  });
}));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.getUserById(id, function(err, user) {
    done(err, user);
  });
});

router.post('/register', upload.single('profileImage'), function(req, res, next) {
  console.log( req.body );
  const name = req.body.name;
  const email = req.body.email;
  const username = req.body.username;
  const password = req.body.password;
  const password2= req.body.password2;

  if(req.file){
    console.log('Uploading File...');
    var profileImage = req.file.filename;
  }else{
    console.log('No File Uploaded...');
    var profileImage = 'noimage.jpg'
  }

  // Form Validation
  req.checkBody('name', 'Name field is required').notEmpty();
  req.checkBody('email', 'Email field is required').notEmpty();
  req.checkBody('email', 'Email is not valid').isEmail();
  req.checkBody('username', 'Username field is required').notEmpty();
  req.checkBody('password', 'Password field is required').notEmpty();
  req.checkBody('password2', 'Passwords do not match').equals(req.body.password);

  // Check Errors
  req.getValidationResult().then(function(result) {
    if(!result.isEmpty()){
      const errors = result.array();
      console.log( errors );
      res.render('register', {
        title: 'Register',
        errors: errors
      });
    }else{
      const newUser = new User({
        name: name,
        email: email,
        username: username,
        password: password,
        profileImage: profileImage
      });

      User.createUser(newUser, function(error, user) {
        if( error ) throw error;
        console.log(user);
      });

      req.flash('success',  'You are now registered and can login');
      res.location('/');
      res.redirect('/');
    }

  });

});

module.exports = router;
